RESTFUL ROUTES

name       url            verb      desc.                                                   Mongoose method
==============================================================================================================
INDEX    /dogs            GET       Display a list of all dogs.                             Dog.find()
NEW      /dogs/new        GET       Show new dog form.                                      N/A
CREATE   /dogs            POST      Add new dog to DB, then redirect somewhere.             Dog.create()
SHOW     /dogs/:id        GET       Shows info about one specific dog.                      Dog.findById()
EDIT     /dogs/:id/edit   GET       Show edit form for one dog.                             Dog.findById()
UPDATE   /dogs/:id        PUT       Update a particular dog, then redirect somewhere.       Dog.findByIdAndUpdate()
DESTROY  /dogs/:id        DELETE    Delete a particular dog, then redirect somewhere.       Dog.findByIdAndRemove()

